from rest_framework import serializers

from users.models import User
from user_registrations.models import UserRegistration
from events.models import Event, RegistryEntry

class EventSerializer(serializers.ModelSerializer):

    class Meta:
        model = Event
        fields = ('id', 'event_name', 'venue', 'price', 'description',
                  'event_date', 'created_date', 'modified_date')


class RegistryEntrySerializer(serializers.ModelSerializer):
    event = EventSerializer()

    class Meta:
        model = RegistryEntry
        fields =('id', 'event', 'unique_id', 'payment_status',
                 'created_date', 'modified_date')

class UserRegistrationAndLoginSerializer(serializers.ModelSerializer):

    class Meta:
        model = UserRegistration
        fields = ('id', 'first_name', 'middle_name', 'last_name', 'wassce_id_number',
                  'date_of_birth', 'phone_number', 'ras_acct_payment_status', 'published_date',
                  'created_date', 'modified', 'user')


class UserSerializer(serializers.ModelSerializer):
    auth_token = serializers.CharField(max_length=500, read_only=True)

    class Meta:
        model = User
        fields =('id','email', 'auth_token')

class UserLoginSerializer(serializers.Serializer):
    auth_token = serializers.CharField(max_length=500, read_only=True)
    email = serializers.CharField(max_length=100)
    userregistration = UserRegistrationAndLoginSerializer()
    registry_entries = RegistryEntrySerializer(many=True)

    class Meta:
        model = User
        fields =('id','email', 'auth_token', 'userregistration', 'registry_entries')

