"""ras URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url
from django.contrib import admin
from django.conf.urls.static import static
from rest_framework.routers import DefaultRouter

# for web login
from django.contrib.auth import views as auth_views

from ras import settings
from users.views import UserViewSet
from users.views import password_reset
from user_registrations.views import UserRegistrationViewSet
from events.views import EventViewSet, MyRegisteredEventsViewSet
from news.views import NewsViewSet
from payment.views import PaymentViewSet
from ras_dashboard.views import *
from rest_framework.authtoken import views
from rest_framework.urlpatterns import format_suffix_patterns
router = DefaultRouter()
router.register('v1/users', UserViewSet)
router.register('v1/registration', UserRegistrationViewSet)
router.register('v1/events', EventViewSet)
router.register('v1/news', NewsViewSet)
router.register('v1/payments', PaymentViewSet)
# router.register('v1/myevents', MyRegisteredEventsViewSet)
router.register('v1/my-events', MyRegisteredEventsViewSet)



urlpatterns = [
    url(r'^jet/', include('jet.urls', 'jet')),  # Django JET URLS
    url(r'^jet/dashboard/', include('jet.dashboard.urls', 'jet-dashboard')),  # Django JET dashboard URLS
    url(r'^admin/', include(admin.site.urls)),
    url(r'^api/', include(router.urls)),
    url(r'^api/v1/users/login/', views.obtain_auth_token),
    url(r'^export_action/', include("export_action.urls", namespace="export_action")),

    # web view login
    url(r'^accounts/login/$', auth_views.login, name='login'),
    url(r'^logout/$', auth_views.logout, name='logout'),
    url(r'^accounts/password_reset/$', auth_views.password_reset,
            {'template_name': 'registration/customized_password_reset_form.html',
            'html_email_template_name': 'registration/customized_password_reset_email.html',
            'subject_template_name': 'registration/customized_password_reset_subject.txt'},
            name='password_reset'),
    url(r'^password_reset/done/$', auth_views.password_reset_done),
    url(r'^reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        auth_views.password_reset_confirm),
    url(r'^accounts/reset/done/$', auth_views.password_reset_complete,
            {'template_name': 'registration/customized_password_reset_complete.html'},
            name='password_reset_complete'),
    # ---------------
    url(r'^accounts/', include('django.contrib.auth.urls')),

    # Ras Dasboard urls.
    url(r'^ras-dashboard/welcome', admin_welcome, name='admin_welcome'),
    url(r'^ras-dashboard/add-event', add_event, name='add_event'),
    url(r'^ras-dashboard/event/(?P<pk>\d+)/edit', edit_event, name='edit_event'),
    url(r'^ras-dashboard/event-list', event_list, name='event_list'),
    url(r'^ras-dashboard/add-news', add_news, name='add_news'),
    url(r'^ras-dashboard/news/(?P<pk>\d+)/edit', edit_news, name='edit_news'),
    url(r'^ras-dashboard/news-list', news_list, name='news_list'),
    url(r'^ras-dashboard/registered-users', registered_users, name='registered_users_list')


] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
