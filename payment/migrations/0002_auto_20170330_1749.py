# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-03-30 17:49
from __future__ import unicode_literals

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('payment', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='payment',
            name='created_date',
            field=models.DateTimeField(auto_now_add=True, default=datetime.datetime(2017, 3, 30, 17, 49, 55, 257455, tzinfo=utc)),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='payment',
            name='modified',
            field=models.DateTimeField(auto_now=True),
        ),
    ]
