from django.shortcuts import render
from rest_framework import viewsets
from django.conf import settings
from rest_framework.decorators import list_route, detail_route
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework import status
from serializers import PaymentSerializer
from rest_framework.response import Response
from rest_framework.renderers import StaticHTMLRenderer
import requests

from django.db.models.signals import post_save


from models import Payment
from user_registrations.models import UserRegistration
from events.models import RegistryEntry
from django.core.mail import EmailMessage

# sending emails
from django.core.mail import send_mail
from django.db.models.signals import post_save
from django.template.loader import get_template
from django.dispatch import receiver
from django.template import Context
from django.core.mail import EmailMultiAlternatives
from payment.emails import ras_account_payment_success_emails
class PaymentViewSet(viewsets.ReadOnlyModelViewSet):
    permission_classes = (IsAuthenticated, )
    serializer_class = PaymentSerializer
    queryset = Payment.objects.all()
    renderer_classes = (StaticHTMLRenderer,)

    def list(self, request, *args, **kwargs):
        payments = request.user.payment_set.all()
        payments = self.serializer_class(payments, many=True)
        return Response({"status_code": status.HTTP_200_OK,
                         "message": "all your payment",
                         "result": payments.data
                         })

    @detail_route(methods=("POST", "GET"), permission_classes=(AllowAny, ))
    def complete(self, request, pk=None, *args, **kwargs):
        payment = self.get_object()
        payment_user = payment.user
        user_reg = UserRegistration.objects.get(user=payment.user)
        status_code = request.GET["status_code"]
        status_message = request.GET["status_message"]
        trans_ref_no = request.GET["trans_ref_no"]
        order_id = request.GET["order_id"]
        signature = request.GET["signature"]

        if status_code == "1":
            payment.payment_status = "cp"
            # payment.is_ras_account=True
            payment.status_message = status_message
            payment.signature = signature
            payment.transaction_no=trans_ref_no
            payment.save()

            if payment.is_ras_account and user_reg:
                user_reg.ras_acct_payment_status = "paid"
                user_reg.save()
                mesg = '<html><body><h3>Ras account payment successful</h3></body></html>'
                # email function
                ras_account_payment_success_emails(user_reg)
                return Response(mesg)

            else:
                RegistryEntry.objects.create(
                    event=payment.event,
                    user=payment.user,
                    author=payment.user,
                    payment_status = "paid"
                )
                mesg = '<html><body><h3>Event payment successful</h3></body></html>'
                return Response(mesg)

        else:
            payment.payment_status = "fa"
            payment.status_message = status_message
            payment.signature = signature
            payment.transaction_no=trans_ref_no
            payment.save()
            mesg = '<html><body><h3>Payment failed</h3></body></html>'
            return Response(mesg)







