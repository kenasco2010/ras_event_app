from rest_framework import serializers
from models import Payment

class PaymentSerializer(serializers.ModelSerializer):

    class Meta:
        model = Payment
        fields = ('id', 'amount', 'is_ras_account', 'event',
          'payment_status', 'user', 'status_message', 'transaction_no',
          'token', 'created_date', 'modified')
