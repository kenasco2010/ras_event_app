# sending emails
from django.core.mail import send_mail
from django.db.models.signals import post_save
from django.template.loader import get_template
from django.dispatch import receiver
from django.template import Context
from django.core.mail import EmailMultiAlternatives
from django.conf import settings
import datetime


def send_email(html_template, plaintext_template, email_subject, context, to):
    # send email after payment

    htmly = get_template(html_template)
    plaintext = get_template(plaintext_template)
    d = Context(context)
    to=to
    text_content = plaintext.render(d)
    html_content = htmly.render(d)
    msg = EmailMultiAlternatives(email_subject, text_content,
                               settings.EMAIL_HOST_USER, [to])
    msg.attach_alternative(html_content, "text/html")
    msg.send()


def ras_account_payment_success_emails(user_reg):
    # Notify user he has Registered for the account
    send_email(
        html_template='sendEmails/send_registration_email_to_ras_user.html',
        plaintext_template='sendEmails/send_registration_email_to_ras_user.txt',
        email_subject='RAS Ghana Account Registration',
        to=user_reg.user.email,
        context={
            'first_name': user_reg.first_name,
            'last_name': user_reg.last_name,
            'date_joined': datetime.datetime.now().date()
        }
    )

    # Notify user he has Paid for the account
    send_email(
        html_template='sendEmails/send_account_payment_info_to_user.html',
        plaintext_template='sendEmails/send_account_payment_info_to_user.txt',
        email_subject='RAS Ghana Account Payment',
        to=user_reg.user.email,
        context={
            'first_name': user_reg.first_name,
            'last_name': user_reg.last_name,
            'date_joined': datetime.datetime.now().date()
        }
    )


# Notify ras ghana a user has Registered for the account
    send_email(
        html_template='sendEmails/send_registration_info_to_ras.html',
        plaintext_template='sendEmails/send_registration_info_to_ras.txt',
        email_subject='RAS Ghana Account Registration',
        to=settings.EMAIL_HOST_USER,
        context={
            'first_name': user_reg.first_name,
            'last_name': user_reg.last_name,
            'date_joined': datetime.datetime.now().date()
        }
    )

    # Notify ras ghana a user has Paid for the account
    send_email(
        html_template='sendEmails/send_account_payment_info_to_ras.html',
        plaintext_template='sendEmails/send_account_payment_info_to_ras.txt',
        email_subject='RAS Ghana Account Payment',
        to=settings.EMAIL_HOST_USER,
        context={
            'first_name': user_reg.first_name,
            'last_name': user_reg.last_name,
            'date_joined': datetime.datetime.now().date()
        }
    )



