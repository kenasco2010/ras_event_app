from __future__ import unicode_literals
from django.utils import timezone
from django.db import models
from django.conf import settings
from events.models import Event

AUTH_USER_MODEL = getattr(settings, 'AUTH_USER_MODEL', 'auth.User')



PAYMENT_STATUSES = (
    ('in', 'initialized'),
    ('fa', 'failed'),
    ('ca', 'canceled'),
    ('ip', 'in progress'),
    ('cp', 'completed'),
)

class Payment(models.Model):
    amount = models.DecimalField(max_digits=5, decimal_places=2)
    is_ras_account = models.BooleanField(default=False)
    event = models.ForeignKey(Event, null=True, blank=True, related_name='payments')
    payment_status = models.CharField(max_length=2, choices=PAYMENT_STATUSES, default='in')
    user = models.ForeignKey(
        AUTH_USER_MODEL, related_name='payments')
    status_message = models.CharField(max_length=100, blank=True)
    transaction_no = models.CharField(max_length=100, blank=True)
    token = models.CharField(max_length=128, blank=True)
    signature = models.CharField(max_length=128, blank=True)
    published_date = models.DateTimeField(blank=True, null=True)
    created_date = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    def publish(self):
        self.published_date = timezone.now()
        self.save()

    def __unicode__(self):
        return '%s %s' % (self.user.email, str(self.amount))

    def __str__(self):
        return '%s %s' % (self.user.email, str(self.amount))


# class RasAccountPayment(models.Model):
#     amount = models.DecimalField(max_digits=5, decimal_places=2)
#     is_ras_account = models.BooleanField(default=False)
#     payment_status = models.CharField(max_length=2, choices=PAYMENT_STATUSES, default='in')
#     user = models.ForeignKey(
#         AUTH_USER_MODEL, related_name='rasaccountpayments')
#     status_message = models.CharField(max_length=100, blank=True)
#     transaction_no = models.CharField(max_length=100, blank=True)
#     token = models.CharField(max_length=128, blank=True)
#     signature = models.CharField(max_length=128, blank=True)
#     created_date = models.DateTimeField(auto_now_add=True)
#     modified = models.DateTimeField(auto_now=True)

#     def publish(self):
#         self.published_date = timezone.now()
#         self.save()

#     def __unicode__(self):
#         return '%s %s' % (self.user.email, str(self.amount))

#     def __str__(self):
#         return '%s %s' % (self.user.email, str(self.amount))
