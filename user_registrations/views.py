import logging

from django.shortcuts import render
from django.conf import settings
from rest_framework import viewsets, serializers
from user_registrations.permissions import IsOwner
from rest_framework.permissions import IsAuthenticated
from serializers import UserRegistrationSerializer
from rest_framework.decorators import list_route, detail_route
from rest_framework.response import Response
from rest_framework import status
import requests
# Create your views here.
from models import UserRegistration
from payment.models import Payment
from payment.serializers import PaymentSerializer
import urllib

from django.core.mail import send_mail
from django.db.models.signals import post_save
from django.template.loader import get_template
from django.dispatch import receiver
from django.template import Context
from django.core.mail import EmailMultiAlternatives


logger = logging.getLogger('ras')


class UserRegistrationViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated, )
    serializer_class = UserRegistrationSerializer
    queryset = UserRegistration.objects.all()

    # The create registration endpoint handles both create and update of the registration.
    @list_route(methods=('POST',), url_path="create-registration", permission_classes = (IsAuthenticated, ))
    def register_user(self, request, *args, **kwargs):
        try:
            get_user_reg = UserRegistration.objects.get(user=request.user)
            get_user_reg.first_name=request.data.get('first_name')
            get_user_reg.middle_name=request.data.get('middle_name')
            get_user_reg.last_name=request.data.get('last_name')
            get_user_reg.wassce_id_number=request.data.get('wassce_id_number')
            get_user_reg.date_of_birth=request.data.get('date_of_birth')
            get_user_reg.phone_number=request.data.get('phone_number')
            get_user_reg.save()
            user_reg = self.serializer_class(get_user_reg)
            return Response({"status_code": status.HTTP_200_OK,
                             "message": "You have updated your registration details",
                             "result": user_reg.data
                             })
        except UserRegistration.DoesNotExist:
            create_user = UserRegistration.objects.create(
                first_name=request.data.get('first_name'),
                middle_name=request.data.get('middle_name'),
                last_name=request.data.get('last_name'),
                wassce_id_number=request.data.get('wassce_id_number'),
                date_of_birth=request.data.get('date_of_birth'),
                phone_number=request.data.get('phone_number'),
                user=request.user
            )
            create_user = self.serializer_class(create_user)
            return Response({"status_code": status.HTTP_201_CREATED,
                             "message": "you have successfully registered",
                             "result": create_user.data
                            })

    # Edit user registration when that user is logged in (redundant code)
    @list_route(methods=('POST',), url_path="edit-registered-user", permission_classes=(IsOwner, ))
    def edit_registered_user(self, request, pk=None, *args, **kwargs):
        user = request.user
        if user:
            registered_user = UserRegistration.objects.get(user=user)
            registered_user.first_name = request.data.get("first_name")
            registered_user.middle_name = request.data.get('middle_name')
            registered_user.last_name = request.data.get('last_name')
            registered_user.wassce_id_number = request.data.get('wassce_id_number')
            registered_user.date_of_birth = request.data.get('date_of_birth')
            registered_user.phone_number = request.data.get('phone_number')
            registered_user.ras_acct_payment_status = request.data.get('ras_acct_payment_status')
            registered_user.save()
            registered_user = self.serializer_class(registered_user)
            return Response({"status_code": status.HTTP_201_CREATED,
                             "message": "You have updated your credentials",
                             "result": registered_user.data
                             })
        else:
            return Response({"status_code": status.HTTP_403_FORBIDDEN,
                             "message": "Updating user registration failed"
                             })


    @list_route(methods=('GET',), url_path="my-registration-details",
                permission_classes=(IsOwner, ))
    def get_loggedin_registered_user(self, request, *args, **kwargs):
          try:
            get_registered_user = UserRegistration.objects.get(user=request.user)
            get_registered_user = self.serializer_class(get_registered_user)
            return Response({"status_code": status.HTTP_200_OK,
                             "message":"your registration details",
                             "result": get_registered_user.data
                             })
          except UserRegistration.DoesNotExist:
                return Response({"status_code": status.HTTP_404_NOT_FOUND,
                                 "message": "Sorry you have not registered."
                                 })

    @list_route(methods=('POST',), url_path='pay-for-ras-account', permission_classes=(IsOwner,))
    def make_ras_acct_payment(self, request, *args, **kwargs):
        ras_account = UserRegistration.objects.get(user=request.user)
        user = request.user
        payment = Payment.objects.create(
            amount = "1.00",
            user = user,
            is_ras_account=True
            )
        request_body = {
            "app_id": settings.INTERPAY_APP_ID,
            "app_key": settings.INTERPAY_APP_KEY,
            "name": ras_account.first_name + " " + ras_account.last_name,
            "mobile": ras_account.phone_number,
            "Order_id": payment.id,
            "email": user.email,
            "currency": "GHS",
            "Amount": payment.amount,
            "order_desc": "Ras account payment",
            "return_url": settings.HOST + '/api/v1/payments/' + str(payment.id) + '/complete/'
        }
        r = requests.post(settings.INTERPAY_URL + "/interapi/ProcessPayment", data=request_body)
        error_message = "Error processing ras account payment"
        try:
            response = r.json()
            if response["status_code"] == 1:
                token = response["Token"]
                payment.transaction_no = response["trans_ref_no"]
                payment.token = urllib.quote(token.encode('utf-8'))
                payment.payment_status = "ip"
                payment.save()
                serialized_payment = PaymentSerializer(payment)
                return Response(
                    {
                        "redirect_url": settings.INTERPAY_URL + "/interapi/confirmpayment?Token=" + payment.token,
                        "payment": serialized_payment.data
                    },
                    status=status.HTTP_201_CREATED
                )
            payment.payment_status = "failed"
            payment.status_message = response["status_message"]
            payment.save()
        except Exception, e:
            error_message = str(e)

        return Response(
            {
                "message": error_message
            },
            status=status.HTTP_400_BAD_REQUEST
        )




#   Send Email to Ras to when someone registers on the platform
# @receiver(post_save, sender=UserRegistration)
# def send_registration_info_to_ras(sender, instance, *args, **kwargs):
#   try:
#     htmly = get_template('sendEmails/send_registration_info_to_ras.html')
#     plaintext = get_template('sendEmails/send_registration_info_to_ras.txt')
#     d = Context({ 'case_id': instance.id,
#                   'first_name': instance.first_name,
#                   'last_name': instance.last_name,
#                   'date_joined': instance.created_date
#                   })
#     email_subject = 'Ras Account registration'
#     # replace email with ras account email for production
#     to=settings.EMAIL_HOST_USER
#     text_content = plaintext.render(d)
#     html_content = htmly.render(d)
#     msg = EmailMultiAlternatives(email_subject, text_content,
#                                  settings.EMAIL_HOST_USER, [to])
#     msg.attach_alternative(html_content, "text/html")
#     msg.send()
#   except Exception, e:
#     logger.exception(e)



# #   Send Email to  user after signing up on the platform
# @receiver(post_save, sender=UserRegistration)
# def send_email_to_ras_user(sender, instance, *args, **kwargs):
#     htmly = get_template('sendEmails/send_email_to_ras_user.html')
#     plaintext = get_template('sendEmails/send_email_to_ras_user.txt')
#     d = Context({ 'case_id': instance.id,
#                   'first_name': instance.first_name,
#                   'last_name': instance.last_name,
#                   'date_joined': instance.created_date
#                   })
#     email_subject = 'Ras Account registration'
#     # replace email with ras account email for production
#     to=instance.user.email
#     text_content = plaintext.render(d)
#     html_content = htmly.render(d)
#     msg = EmailMultiAlternatives(email_subject, text_content,
#                                  settings.EMAIL_HOST_USER, [to])
#     msg.attach_alternative(html_content, "text/html")
#     msg.send()
