from __future__ import unicode_literals

from django.apps import AppConfig


class UserRegistrationsConfig(AppConfig):
    name = 'user_registrations'
