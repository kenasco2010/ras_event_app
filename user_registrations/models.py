from __future__ import unicode_literals
from django.utils import timezone

from django.db import models
from users.models import User
from django.conf import settings
from datetime import date

AUTH_USER_MODEL = getattr(settings, 'AUTH_USER_MODEL', 'auth.User')



class UserRegistration(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    first_name = models.CharField(max_length=15, blank=True)
    middle_name = models.CharField(max_length=15, blank=True)
    last_name = models.CharField(max_length=15, blank=True)
    wassce_id_number = models.CharField(max_length=20, blank=True)
    date_of_birth = models.DateField(("Date"), default=date.today, null=True, blank=True)
    phone_number = models.CharField(max_length=20, blank=True)
    ras_acct_payment_status = models.CharField(max_length=15, blank=True, default="unpaid")
    published_date = models.DateTimeField(blank=True, null=True)
    created_date = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    def publish(self):
        self.published_date = timezone.now()
        self.save()

    def __unicode__(self):
        return '%s %s' % (self.first_name, self.last_name)

    def __str__(self):
        return '%s %s' % (self.first_name, self.last_name)


