# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-03-20 18:06
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('user_registrations', '0003_auto_20170307_1911'),
    ]

    operations = [
        migrations.AddField(
            model_name='userregistration',
            name='ras_acct_payment_status',
            field=models.CharField(blank=True, default='unpaid', max_length=15),
        ),
    ]
