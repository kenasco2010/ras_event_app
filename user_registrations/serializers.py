from models import UserRegistration
from rest_framework import serializers
from users.models import User


class UserRegistrationSerializer(serializers.ModelSerializer):

    class Meta:
        model = UserRegistration
        fields = ('id', 'first_name', 'middle_name', 'last_name', 'wassce_id_number',
                  'date_of_birth', 'phone_number', 'ras_acct_payment_status', 'published_date',
                  'created_date', 'modified')
