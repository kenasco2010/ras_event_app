from rest_framework import serializers
from models import Event, RegistryEntry


class EventSerializer(serializers.ModelSerializer):

    class Meta:
        model = Event
        fields = ('id', 'event_name', 'venue', 'price', 'description',
                  'event_date_and_time', 'created_date', 'modified_date')


class RegistryEntrySerializer(serializers.ModelSerializer):
    event = EventSerializer()

    class Meta:
        model = RegistryEntry
        fields =('id', 'event', 'user', 'unique_id', 'payment_status',
                 'author', 'created_date', 'modified_date')


# class RegisteredEventSerializer(serializers.ModelSerializer):
#     # unique_id = RegistryEntrySerializer(source='unique_id_set', many=True)
#     event = EventSerializer(many=True)
#     class Meta:
#         model = RegistryEntry
#         fields = ('id', 'event', 'user', 'unique_id', 'payment_status',
#                  'author', 'created_date', 'modified_date')
