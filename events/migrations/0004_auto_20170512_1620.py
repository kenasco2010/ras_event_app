# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-05-12 16:20
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('events', '0003_event_event_date'),
    ]

    operations = [
        migrations.AddField(
            model_name='event',
            name='published_date',
            field=models.DateTimeField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='registryentry',
            name='published_date',
            field=models.DateTimeField(blank=True, null=True),
        ),
    ]
