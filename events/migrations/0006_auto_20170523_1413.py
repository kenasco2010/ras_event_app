# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-05-23 14:13
from __future__ import unicode_literals

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('events', '0005_event_event_time'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='event',
            name='event_date',
        ),
        migrations.RemoveField(
            model_name='event',
            name='event_time',
        ),
        migrations.AddField(
            model_name='event',
            name='event_date_and_time',
            field=models.DateTimeField(default=datetime.datetime(2017, 5, 23, 14, 13, 48, 754994, tzinfo=utc)),
            preserve_default=False,
        ),
    ]
