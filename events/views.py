from django.shortcuts import render
from django.conf import settings
from rest_framework import viewsets
from rest_framework.views import APIView
from serializers import EventSerializer, RegistryEntrySerializer
from payment.serializers import PaymentSerializer
from rest_framework.decorators import list_route, detail_route
from rest_framework.response import Response
from rest_framework import status
from rest_framework.permissions import IsAuthenticated
import requests
from django.db.models.signals import pre_save
from ras.override_pagination import StandardResultsSetPagination

from permissions import IsOwner, IsObjectOwner
# Create your views here.
from payment.models import Payment
from events.models import Event, RegistryEntry
from user_registrations.models import UserRegistration
import urllib
# send emails
from django.core.mail import send_mail
from django.db.models.signals import post_save
from django.template.loader import get_template
from django.dispatch import receiver
from django.template import Context
from django.core.mail import EmailMultiAlternatives


class EventViewSet(viewsets.ModelViewSet):
    # permission_classes = (IsOwner,)
    serializer_class = EventSerializer
    queryset = Event.objects.all().order_by('-id')
    pagination_class = StandardResultsSetPagination

    @list_route(methods=('POST',), url_path="add-event")
    def create_event(self, request, *args, **kwargs):
        user = request.user.is_authenticated()
        if user:
            event = Event.objects.create(
                event_name = request.data.get('event_name'),
                venue = request.data.get('venue'),
                price = request.data.get('price'),
                description = request.data.get('description'),
                event_date = request.data.get('event_date'),
                author = request.user
            )
            event = self.serializer_class(event)
            return Response({"status_code": status.HTTP_201_CREATED,
                             "message": "you have successfully created an event",
                             "result": event.data
                            })
        else:
            return Response({"status_code": status.HTTP_401_UNAUTHORIZED,
                             "message": "operation failed, please check the fields"
                             })

    @detail_route(methods=('POST',), url_path="edit-event")
    def edit_event(self, request, pk=None, *args, **kwargs):
        user = request.user.is_authenticated()
        if user:
            event = self.get_object()
            for field in ['event_name', 'venue', 'price', 'description', 'event_date']:
                setattr(event, field, request.data.get(field))
            event.save()
            event = self.serializer_class(event)
            return Response({"status_code": status.HTTP_201_CREATED,
                             "message": "you have successfully updated an event",
                             "result": event.data
                            })
        else:
            return Response({"status_code": status.HTTP_403_FORBIDDEN,
                            "message": "Updating event failed"
                            })

    # @receiver(post_save, sender=User)
    # def save_event_registry(sender, instance, **kwargs):


    # Allows users to register for one event after payment.
    @detail_route(methods=('POST',), url_path="register-for-event", permission_classes=(IsAuthenticated, ))
    def register_for_event(self, request, *args, **kwargs):
        user = request.user
        event = self.get_object()
        event_id = event.id
        # payment_status = RegistryEntry.objects.get(payment_status = "paid")
        event_registered = RegistryEntry.objects.filter(user=user)
        if user:
            if not event_registered:
                register_event = RegistryEntry.objects.create(
                    event=event,
                    user=request.user,
                    payment_status=request.data.get('payment_status'),
                    author=request.user
                )
                register_event = RegistryEntrySerializer(register_event)
                return Response({"status_code": status.HTTP_201_CREATED,
                                 "message": "you have successfully registered for an event",
                                 "result": register_event.data
                                 })
            else:
                return Response({"status_code": status.HTTP_401_UNAUTHORIZED,
                                 "message": "You have already registered for this event"
                                 })
        else:
            return Response({"status_code": status.HTTP_400_BAD_REQUEST,
                             "message": "Unsuccessful event registration, "
                                        "check your payment status"
                             })



    @detail_route(methods=('GET',), url_path="is-registered", permission_classes=(IsAuthenticated,))
    def is_registered(self, request,pk=None, *args, **kwargs):
        event = self.get_object()

        try:
            event = RegistryEntry.objects.get(event=event, user=request.user)
            event = RegistryEntrySerializer(event)
            return Response({"status_code": status.HTTP_200_OK,
                          "message": "Event is registered for",
                         "result": event.data
                        })

        except RegistryEntry.DoesNotExist:
            pass

        return Response({"status_code": status.HTTP_404_NOT_FOUND,
                         "message": "Event is not registered",
                         "result": None
                        },
                        status=status.HTTP_404_NOT_FOUND
                        )

    @detail_route(methods=('POST',), url_path="make-event-payment", permission_classes=(IsAuthenticated,))
    def make_event_payment(self, request, pk=None, *args, **kwargs):
        event = self.get_object()
        try:
          event = RegistryEntry.objects.get(user=request.user, event=event)
          if event:
            return Response({"message": "you have paid and registered for the event already"})
        except RegistryEntry.DoesNotExist:
            pass
        user =request.user
        payment = Payment.objects.create(
            amount = event.price,
            event = event,
            user = request.user,
            is_ras_account=False

            )
        request_body = {
            "app_id": settings.INTERPAY_APP_ID,
            "app_key": settings.INTERPAY_APP_KEY,
            "name": request.user.userregistration.first_name + " " + request.user.userregistration.last_name,
            "mobile": request.user.userregistration.phone_number,
            "Order_id": payment.id,
            "email": user.email,
            "currency": "GHS",
            "Amount": payment.amount,
            "order_desc": event.event_name,
            "return_url": settings.HOST + '/api/v1/payments/' + str(payment.id) + '/complete/'
        }

        r = requests.post(settings.INTERPAY_URL + "/interapi/ProcessPayment", data=request_body)
        error_message = "Error proccessing payment"

        try:
            response = r.json()
            if response["status_code"] == 1:
                token = response["Token"]
                payment.transaction_no = response["trans_ref_no"]
                payment.token =  urllib.quote(token.encode('utf-8'))
                payment.payment_status = "ip"
                payment.save()

                serialized_payment = PaymentSerializer(payment)

                return Response(
                    {
                        "redirect_url": settings.INTERPAY_URL + "/interapi/confirmpayment?Token=" + payment.token,
                        "payment": serialized_payment.data
                    },
                    status=status.HTTP_201_CREATED
                )
            payment.payment_status = "failed"
            payment.status_message = response["status_message"]
            payment.save()
        except Exception, e:
            error_message = str(e)

        return Response(
            {
                "message": error_message
            },
            status=status.HTTP_500_INTERNAL_SERVER_ERROR
        )

class MyRegisteredEventsViewSet(viewsets.ReadOnlyModelViewSet):
    serializer_class = RegistryEntrySerializer
    queryset = RegistryEntry.objects.all()
    permission_classes = (IsObjectOwner,)




#   Send Email to Ras to when someone pays for an event on the platform
@receiver(post_save, sender=RegistryEntry)
def send_event_payment_details_to_ras(sender, instance, *args, **kwargs):
    htmly = get_template('sendEmails/send_event_payment_details_to_ras.html')
    plaintext = get_template('sendEmails/send_event_payment_details_to_ras.txt')
    d = Context({
                'user_email':instance.user.email,
                'event_name': instance.event,
                'event_unique_id': instance.unique_id,
                'event_amount': instance.event.price,
                'event_venue': instance.event.venue,
                'event_description': instance.event.description,
                'event_date': instance.event.event_date_and_time,
                'registry_date': instance.created_date
                  })
    email_subject = 'Ras Event Payment'
    # replace email with ras account email for production
    to=settings.EMAIL_HOST_USER
    text_content = plaintext.render(d)
    html_content = htmly.render(d)
    msg = EmailMultiAlternatives(email_subject, text_content,
                                 settings.EMAIL_HOST_USER, [to])
    msg.attach_alternative(html_content, "text/html")
    msg.send()


#   Send Email to Ras user after Paying for event on the platform
@receiver(post_save, sender=RegistryEntry)
def send_event_payment_email_to_ras_user(sender, instance, *args, **kwargs):
    event_user = UserRegistration.objects.get(user=instance.user)
    htmly = get_template('sendEmails/paid_event_success.html')
    plaintext = get_template('sendEmails/paid_event_success.txt')
    d = Context({ 'user_first_name':event_user.first_name,
                  'event_name': instance.event,
                  'event_unique_id': instance.unique_id,
                  'event_amount': instance.event.price,
                  'event_venue': instance.event.venue,
                  'event_description': instance.event.description,
                  'event_date': instance.event.event_date_and_time,
                  'registry_date': instance.created_date
                  })
    email_subject = str(instance.event) + ' ' +'payment on RAS app'
    # replace email with ras account email for production
    to=instance.user.email
    text_content = plaintext.render(d)
    html_content = htmly.render(d)
    msg = EmailMultiAlternatives(email_subject, text_content,
                                 settings.EMAIL_HOST_USER, [to])
    msg.attach_alternative(html_content, "text/html")
    msg.send()
