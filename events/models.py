from __future__ import unicode_literals
#
from django.utils import timezone
from django.db import models
from ras import settings
from users.models import User
import uuid

# Create your models here.
AUTH_USER_MODEL = getattr(settings, 'AUTH_USER_MODEL', 'auth.User')


def generate_random_case_num():
    random_gen = str(uuid.uuid4().get_hex().upper()[0:7])
    return random_gen


class Event(models.Model):
    event_name = models.CharField(max_length=50)
    venue = models.CharField(max_length=100)
    price = models.IntegerField()
    description = models.TextField()
    # event_date = models.DateField()
    event_date_and_time = models.DateTimeField()
    # event_time = models.TimeField(blank=True, null=True)

    published_date = models.DateTimeField(blank=True, null=True)
    created_date = models.DateTimeField(auto_now_add=True)
    modified_date = models.DateTimeField(auto_now=True)
    author = models.ForeignKey(
        AUTH_USER_MODEL, on_delete=models.CASCADE)

    def publish(self):
        self.published_date = timezone.now()
        self.save()

    def __unicode__(self):
        return self.event_name

    def __str__(self):
        return self.event_name


class RegistryEntry(models.Model):
    event = models.ForeignKey(
        Event, related_name="registry_entries")
    user = models.ForeignKey(
        AUTH_USER_MODEL, on_delete=models.CASCADE,
        related_name="registry_entries")
    unique_id = models.CharField(
        max_length=7, default=generate_random_case_num)
    payment_status = models.CharField(max_length=15, blank=True)
    author = models.ForeignKey(
        AUTH_USER_MODEL, on_delete=models.CASCADE)

    published_date = models.DateTimeField(blank=True, null=True)
    created_date = models.DateField(auto_now_add=True)
    modified_date = models.DateField(auto_now=True)

    def publish(self):
        self.published_date = timezone.now()
        self.save()

    def __unicode__(self):
        return self.event.event_name

    def __str__(self):
        return self.event.event_name
