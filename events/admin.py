from django.contrib import admin

# Register your models here.
from models import Event, RegistryEntry


mybackendContentModels = [Event, RegistryEntry]
admin.site.register(mybackendContentModels)
