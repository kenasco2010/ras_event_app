from __future__ import unicode_literals
from django.utils import timezone
from django.db import models

from ras import settings
# Create your models here.

AUTH_USER_MODEL = getattr(settings, 'AUTH_USER_MODEL', 'auth.User')


class News(models.Model):
    title = models.CharField(max_length=200, blank=True)
    description = models.TextField()

    published_date = models.DateTimeField(blank=True, null=True)
    created_date = models.DateField(auto_now_add=True)
    modified_date = models.DateField(auto_now=True)
    author = models.ForeignKey(
        AUTH_USER_MODEL, on_delete=models.CASCADE)

    def publish(self):
        self.published_date = timezone.now()
        self.save()

    def __unicode__(self):
        return self.title

    def __str__(self):
        return self.title

    class Meta:
        verbose_name_plural = "News"
