from rest_framework import viewsets
from rest_framework.decorators import list_route
from rest_framework.permissions import AllowAny
from serializers import NewsSerializer

from models import News
from ras.override_pagination import StandardResultsSetPagination

class NewsViewSet(viewsets.ModelViewSet):
    permission_classes = (AllowAny,)
    model = News
    serializer_class = NewsSerializer
    queryset = News.objects.all()
    pagination_classes = StandardResultsSetPagination
