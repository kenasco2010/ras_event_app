from django import forms
from django.forms import TextInput
from events.models import Event
from news.models import News
import datetime
from django.forms.widgets import *

class EventForm(forms.ModelForm):
    event_name = forms.CharField(
            label = 'Name of event',
            widget=forms.TextInput(attrs={'class': "form-control col-sm-6"}),
    )
    description = forms.CharField(
            label = "Event description",
            widget = forms.Textarea(attrs={'class': "form-control", 'rows': 5, 'cols': 20, 'id':'project_description'}),
    )
    venue = forms.CharField(
            label = 'Venue of event',
            widget=forms.TextInput(attrs={'class': "form-control"}),
    )
    price = forms.CharField(
            label = 'Price of event',
            widget=forms.TextInput(attrs={'class': "form-control"}),
    )
    event_date_and_time = forms.DateTimeField(
            required=False,
            label = 'Date and time of event',
            widget = forms.TextInput(attrs={'class': "form-control"}),
    )
    # time = forms.CharField(
    #         label = 'Time',
    #         widget=forms.TextInput(attrs={'class': "form-control"}),
    # )

    # event_date_and_time = forms.DateTimeField(
    #     input_date_formats=['%d/%m/%Y'],
    #     input_time_formats=['%H:%M'],
    #     widget=SplitDateTimeWidget(date_format='%d/%m/%Y',
    #     time_format='%H:%M'),
    #     )
    class Meta:
        model = Event
        fields = ['event_name' ,'description', 'venue', 'price', 'event_date_and_time']
    # separate_time = forms.TimeField(input_formats=['%H:%M'])
    # separate_date = forms.DateField(input_formats=['%d.%m.%Y'])


class NewsForm(forms.ModelForm):
    title = forms.CharField(
            label = 'Title of News',
            widget=forms.TextInput(attrs={'class': "form-control col-sm-6"}),
    )
    description = forms.CharField(
            label = "News description",
            widget = forms.Textarea(attrs={'class': "form-control", 'rows': 5, 'cols': 20, 'id':'project_description'}),
    )

    class Meta:
        model = News
        fields = ['title' ,'description']
