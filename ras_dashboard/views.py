from django.shortcuts import render, get_object_or_404
from ras_dashboard.forms import *
from events.models import *
from user_registrations.models import *
from django.contrib import messages
import datetime
from django.shortcuts import redirect, render
from django.http import HttpResponse, HttpResponseRedirect

# Create your views here.

def admin_welcome(request):
  return render(request, 'ras_dashboard/welcome.html')

def add_event(request):
    add_event_form = EventForm(request.POST or None)
    success = False
    if request.method == 'POST':
        if add_event_form.is_valid():
            date = request.POST['registration_date']
            time = request.POST['registration_time']
            add_event_form = add_event_form.save(commit=False)
            add_event_form.author = request.user
            add_event_form.published_date = timezone.now()
            # print datetime.combine(date, time)
            add_event_form.save()
            add_event_form = EventForm()
            success = True

            messages.add_message(request, messages.INFO, 'Event saved successfully')
    else:
        add_event_form = EventForm()
    return render(request, 'ras_dashboard/add_event.html', {'form': add_event_form})


def edit_event(request, pk):
    event = get_object_or_404(Event, pk=pk)
    if request.method == 'POST':
        event_form = EventForm(request.POST, instance=event)
        if event_form.is_valid():
            date = request.POST['registration_date']
            time = request.POST['registration_time']
            event_form = event_form.save(commit=False)
            event_form.author = request.user
            event_form.published_date = timezone.now()
            # print datetime.combine(date, time)
            event_form.save()
            return redirect('event_list')

            messages.add_message(request, messages.INFO, 'Event saved successfully')
    else:
        event_form = EventForm(instance=event)
    return render(request, 'ras_dashboard/edit_event.html', {'form': event_form})


def event_list(request):
    list_of_events = Event.objects.all().order_by('-id')
    context = {
    'event_list': list_of_events
    }
    return render(request, 'ras_dashboard/view_events_list.html', context)

def add_news(request):
    add_news_form = NewsForm(request.POST or None)
    success = False
    if request.method == 'POST':
        if add_news_form.is_valid():
            add_news_form = add_news_form.save(commit=False)
            add_news_form.author = request.user
            add_news_form.published_date = timezone.now()
            add_news_form.save()
            add_news_form = NewsForm()
            success = True
            messages.add_message(request, messages.INFO, 'News saved successfully')
    else:
        add_news_form = NewsForm()
    return render(request, 'ras_dashboard/add_news.html', {'form': add_news_form})


def edit_news(request, pk):
    news = get_object_or_404(News, pk=pk)
    if request.method == 'POST':
        news_form = NewsForm(request.POST, instance=news)
        if news_form.is_valid():
            news_form = news_form.save(commit=False)
            news_form.author = request.user
            news_form.published_date = timezone.now()
            news_form.save()
            return redirect('news_list')

            messages.add_message(request, messages.INFO, 'News edited successfully')
    else:
        news_form = NewsForm(instance=news)
    return render(request, 'ras_dashboard/edit_news.html', {'form': news_form})


def news_list(request):
    list_of_news = News.objects.all().order_by('-id')
    context = {
    'news_list': list_of_news
    }
    return render(request, 'ras_dashboard/view_news_list.html', context)


def registered_users(request):
    reg_users = UserRegistration.objects.all().order_by('-id')
    context = {
    'user_registration': reg_users
    }
    return render(request, 'ras_dashboard/registered_users.html', context)
